# Análisis de sentimientos en las elecciones 2021 de la segunda vuelta del Perú
<br>

<br>
Crearemos un software, que tendrá como objetivo analizar los sentimientos de personas que comentan en Twitter con respecto a las elecciones presidenciales de 2021 en el Perú. Este software tendrá la opción de generar estadísticas de los sentimientos generados por candidato para visualizar la aceptación o desaprobación del público, de este modo obtendremos información sobre los sentimientos del público por las elecciones presidenciales en general o acerca de un candidato en específico.

<br>
<br>

# Pasos
1. npm install
2. npm start
<br>
<br>


